package kr.co.m114.common.security;

/** RoleType.java
 * 생성일	: 2019. 5. 3.
 * 설명		: 권한 정보
 * @author sikim
 * @version 1.0
 */
public final class RoleType {
	public static final String UNDEFINED = "";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";
}
