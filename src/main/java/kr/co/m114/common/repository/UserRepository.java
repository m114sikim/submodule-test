package kr.co.m114.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kr.co.m114.common.domain.user.User;

/** UserRepository.java
 * 생성일	: 2019. 5. 13.
 * 설명		: - 
 * @author sikim
 * @version 1.0
 */
public interface UserRepository extends JpaRepository<User, Integer> {
	public User findById(String id);
}
