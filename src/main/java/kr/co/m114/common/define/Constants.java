package kr.co.m114.common.define;

/** Constants.java
 * 생성일	: 2019. 2. 13.
 * 설명		: - 
 * @author sikim
 * @version 1.0
 */
public final class Constants {

	public final static class CHANGE_TYPE {
		public final static String REG = "reg";
		public final static String MOD = "mod";
		public final static String DEL = "del";
	}

	public final static class USE {
		public final static String Y = "Y";
		public final static String N = "N";
	}

	public final static class Page {
		public final static Integer DEFAULT_SIZE = 10;
	}
}
