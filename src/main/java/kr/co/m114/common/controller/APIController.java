package kr.co.m114.common.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;

import kr.co.m114.base.controller.BaseController;
import kr.co.m114.base.util.DateUtil;
import kr.co.m114.common.domain.user.User;
import kr.co.m114.common.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;

/** 
 * APIController.java
 * 생성일	: 2019. 1. 21.
 * 설명		: 기본 api 컨트롤러
 * @author sikim
 * @version 1.0
 */
@Slf4j
@RequestMapping("/api")
@RestController
@CrossOrigin
public class APIController extends BaseController {
	
	@GetMapping("/now")
	public @ResponseBody Map<?, ?> getNow() throws Exception {
		String now = DateUtil.getNowToString();

		return ImmutableMap.<String, Object>builder()
				.put("now", now)
				.build();
	}
	
	@Autowired
	private UserServiceImpl userService;
	
	@GetMapping("/users")
	public @ResponseBody Map<?, ?> getUsers() throws Exception {
		List<User> list = userService.getUser();
		log.debug("list: {}", list);

		return ImmutableMap.<String, Object>builder()
				.put("list", list)
				.build();
	}
}
