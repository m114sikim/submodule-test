package kr.co.m114.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/** MappingController.java
 * 생성일	: 2019. 4. 16.
 * 설명		: URL 접근 시 Vue-router의 history 모드 주소와 화면을 매핑시킨다
 * 			: 참조 - https://febdy.tistory.com/75
 * @author sikim
 * @version 1.0
 */
@Controller
public class MappingController {
	
	// 관리자 페이지
	@GetMapping(value={
			"/admin/**/{path:[^\\\\.]+}",
//			"/admin"
	})
	public String forwardToAdmin() {
		return "forward:/admin.html";
	}
	
	// 사용자 페이지
	@GetMapping(value="/{path:[^\\\\.]*}")
	public String forwardToIndex(@PathVariable("path") String path) {
		if ("admin".equals(path)) {
			return "forward:/admin.html";
		} else {
			return "forward:/index.html";
		}
	}
}
