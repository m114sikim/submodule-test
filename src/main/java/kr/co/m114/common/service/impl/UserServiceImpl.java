package kr.co.m114.common.service.impl;

import static kr.co.m114.common.security.RoleType.ROLE_ADMIN;
import static kr.co.m114.common.security.RoleType.ROLE_USER;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import kr.co.m114.base.service.BaseService;
import kr.co.m114.common.domain.user.User;
import kr.co.m114.common.repository.UserRepository;

/** UserServiceImpl.java
 * 생성일	: 2019. 5. 13.
 * 설명		: - 
 * @author sikim
 * @version 1.0
 */
@Service
public class UserServiceImpl extends BaseService implements UserDetailsService {

	@Autowired
	public UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findById(username); //.orElseThrow(() -> new UsernameNotFoundException(String.format("id: %s", username)));
		// 기본 권한
		user.grantAuthority(ROLE_USER);
		// 추가 권한
		if (ROLE_ADMIN.equals(user.getRoleType())) {
			user.grantAuthority(ROLE_ADMIN);
		}
		return user;
	}
	
	public List<User> getUser() throws Exception {
		List<User> list = userRepository.findAll();
		return list;
	}

}
