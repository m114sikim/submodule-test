package kr.co.m114.common.domain.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** User.java
 * 생성일	: 2019. 5. 3.
 * 설명		: 사용자 정보
 * @author sikim
 * @version 1.0
 */
@Data
@Entity(name = "user")
@Table(name = "test_tb_user")
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(value= {"authorities"})
public class User implements UserDetails {
	private static final long serialVersionUID = 8059760079693201300L;

	@Id
	@Column
	@EqualsAndHashCode.Include
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TB_USER")
	@SequenceGenerator(name = "SEQ_TB_USER", sequenceName = "SEQ_TB_USER", allocationSize = 1)
	private Integer seq;
	@Column(length = 100, nullable = false)
	private String id;
	@JsonIgnore
	@Column(length = 100, nullable = false)
	private String password;
	@Column(length = 1, nullable = false)
	@Convert(converter=RoleTypeConverter.class)
	private String roleType;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date regDate;
	@Column(length = 20, nullable = false)
	private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    @Transient private List<String> roles;
    
	@Transient private boolean accountNonExpired;
	@Transient private boolean accountNonLocked;
	@Transient private boolean credentialsNonExpired;
	@Transient private boolean enabled;
	
	// 사용자 IP
	@Transient
	private String ip;
	
    public User() {
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;		
	}

    public void grantAuthority(String authority) {
        if ( roles == null ) roles = new ArrayList<>();
        roles.add(authority);
    }

    @Override
    public List<GrantedAuthority> getAuthorities(){
        List<GrantedAuthority> authorities = new ArrayList<>();
        roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
        return authorities;
    }

	@Override
	public String getUsername() {
		return this.getId();
	}

}
