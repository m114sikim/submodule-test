package kr.co.m114.common.domain.user;

import javax.persistence.AttributeConverter;

import kr.co.m114.common.security.RoleType;


/** RoleTypeConverter.java
 * 생성일	: 2019. 5. 3.
 * 설명		: - 
 * @author sikim
 * @version 1.0
 */
public class RoleTypeConverter implements AttributeConverter<String, String> {
	private static class RoleTypeCode {
		private final static String ROLE_USER = "1";		// 사용자
		private final static String ROLE_ADMIN = "2";		// 관리자
	}
	
	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
	 */
	@Override
	public String convertToDatabaseColumn(String v) {
		if (RoleType.ROLE_ADMIN.equals(v))			return RoleTypeCode.ROLE_ADMIN;
		else if (RoleType.ROLE_USER.equals(v))		return RoleTypeCode.ROLE_USER;
		else										return "";
	}

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
	 */
	@Override
	public String convertToEntityAttribute(String c) {
		if (RoleTypeCode.ROLE_ADMIN.equals(c))			return RoleType.ROLE_ADMIN;
		else if (RoleTypeCode.ROLE_USER.equals(c))		return RoleType.ROLE_USER;
		else											return RoleType.UNDEFINED;
	}

}
