package kr.co.m114.common.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/** 
 * EmbeddedServletContainerCustomize.java
 * 생성일	: 2019. 2. 13.
 * 설명		: vue-router 매핑을 위해 넘어온 URL에 대한 페이지를 index.html 페이지로 연결
 * @author sikim
 * @version 1.0
 */
@Component
public class EmbeddedServletContainerCustomize implements EmbeddedServletContainerCustomizer {
	@Override
    public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
        configurableEmbeddedServletContainer.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/index.html"));
    }
}
