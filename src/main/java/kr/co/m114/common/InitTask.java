package kr.co.m114.common;

import java.io.File;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/** 
 * InitTask.java
 * 생성일	: 2019. 4. 3.
 * 설명		: 서버 시작 시 실행하는 작업
 * @author sikim
 * @version 1.0
 */
@Slf4j
@Component
public class InitTask {
	
	@Value("${m114.path.upload}")
	private String UPLOAD_PATH;
    @Value("${m114.path.tmp}")
    private String TMP_FILE_PATH;
	
	@PostConstruct
	public void init() throws Exception {
		log.debug("---------- 서버 초기화 함수를 실행 -----------");
		createFilesFolder();
		createTmpFolder();
	}
	
	private boolean createFilesFolder() {
		log.debug("---------- instanceService.createFilesFolder() -----------");
		boolean res = Optional.of(new File(UPLOAD_PATH)).filter(d -> !d.exists()).map(d -> d.mkdirs()).orElse(true);
		log.debug("---------- {} 생성/확인 완료 -----------", UPLOAD_PATH);
		return res;
	}
	private boolean createTmpFolder() {
		log.debug("---------- instanceService.createTmpFolder() -----------");
		boolean res = Optional.of(new File(TMP_FILE_PATH)).filter(d -> !d.exists()).map(d -> d.mkdirs()).orElse(true);
		log.debug("---------- {} 생성/확인 완료 -----------", TMP_FILE_PATH);
		return res;
	}
}
